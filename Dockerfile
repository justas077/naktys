FROM golang:1.15.2-alpine3.12 AS builder

WORKDIR /go/src/naktys
COPY . .

RUN go build -o main .

EXPOSE 8065

CMD ["./main"]