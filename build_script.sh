#!/bin/bash

SERVICE_NAME=naktys
DOCKER_REPO=justinianas
VERSION=latest

docker build -t $SERVICE_NAME:$VERSION .  -f ./Dockerfile

IMAGE_ID=$(docker image list | grep ^$SERVICE_NAME | grep latest | awk '{print $3}')

docker tag $IMAGE_ID $DOCKER_REPO/$SERVICE_NAME:$VERSION

docker push $DOCKER_REPO/$SERVICE_NAME