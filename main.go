package main

import (
	"github.com/r3labs/sse"
	"log"
	"net/http"
	"regexp"
	"time"
)

const (
	ActionsStream 		= "actions"
	CheckStream 		= "check"

	QueryKeySessionId	= "session"
	QueryKeyAction		= "action"
	QueryKeyData		= "data"

	AddTime				= 12
	CheckTime			= 7
)

var validActions = []string{ "start", "open", "next", "previous" }

type State struct {
	Session		*string
	Until		time.Time
}

var state = State{}
var server *sse.Server

func main() {
	server = sse.New()
	server.CreateStream(ActionsStream)
	server.CreateStream(CheckStream)

	mux := http.NewServeMux()
	mux.HandleFunc("/events", server.HTTPHandler)

	mux.HandleFunc("/init", initConnection)
	mux.HandleFunc("/close", closeConnection)
	mux.HandleFunc("/action", passAction)
	mux.HandleFunc("/alive", setAlive)
	mux.HandleFunc("/clear", clearLog)

	go checkAlive()

	http.ListenAndServe(":8065", mux)
}


func initConnection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if !checkTime() {
		state.Session = nil
	}

	if state.Session == nil {
		if sessionID := getSessionID(r); sessionID != nil {
			state.Session = sessionID
			state.Until = time.Now().Add(time.Second * AddTime)
			log.Println("Session is initialized", *state.Session)
			return
		}
	}

	w.WriteHeader(http.StatusBadRequest)
	return
}


func closeConnection(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	sessionID := getSessionID(r)
	if sessionID != nil && state.Session != nil && *state.Session == *sessionID {
		closeCon()
		return
	}

	w.WriteHeader(http.StatusBadRequest)
	return
}


func passAction(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	action := getQueryParameter(QueryKeyAction, r)
	data := getQueryParameter(QueryKeyData, r)
	sessionID := getSessionID(r)
	if checkTime() && action != nil && sessionID != nil && state.Session != nil && *state.Session == *sessionID && server != nil && contains(validActions, *action) {
		event := &sse.Event{
			Event: []byte(*action),
		}
		if data != nil {
			event.Data = []byte(*data)
		}
		server.Publish(ActionsStream, event)
		log.Println("Action is passed", *action, data)
		return
	}

	w.WriteHeader(http.StatusBadRequest)
	return
}

func setAlive(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	sessionID := getSessionID(r)
	if sessionID != nil && state.Session != nil && *state.Session == *sessionID {
		state.Until = time.Now().Add(time.Second * AddTime)
		log.Println("Extending alive time...", *sessionID, state.Until.Format(time.RFC3339))
		return
	}

	w.WriteHeader(http.StatusBadRequest)
	return
}

func clearLog(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	log.Println("Clearing logs...")
	server.Streams[ActionsStream].Eventlog.Clear()
	server.Streams[CheckStream].Eventlog.Clear()
}

func checkAlive() {
	for {
		time.Sleep(time.Second * CheckTime)
		if !checkTime() && state.Session != nil {
			log.Println("Need to closeCon connection...", *state.Session)
			closeCon()
		}
	}
}

func closeCon() {
	log.Println("Need to closeCon connection...", *state.Session)
	server.Publish(CheckStream, &sse.Event{
		Data: []byte("died\n"),
	})
	state.Session = nil
	state.Until = time.Now().Add(-time.Second)
	server.Streams[ActionsStream].Eventlog.Clear()
	server.Streams[CheckStream].Eventlog.Clear()
}

func getSessionID(r *http.Request) *string {
	sessionID := getQueryParameter(QueryKeySessionId, r)
	if sessionID != nil && isValidUUID(*sessionID) {
		return sessionID
	}
	return nil
}

func getQueryParameter(name string, r *http.Request) *string {
	if keys, ok := r.URL.Query()[name]; ok && keys[0] != "" {
		return &keys[0]
	}
	return nil
}

func isValidUUID(uuid string) bool {
	r := regexp.MustCompile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-4[a-fA-F0-9]{3}-[8|9|aA|bB][a-fA-F0-9]{3}-[a-fA-F0-9]{12}$")
	return r.MatchString(uuid)
}

func contains(array []string, val string) bool {
	for _, v := range array {
		if v == val {
			return true
		}
	}
	return false
}

func checkTime() bool {
	return time.Now().Unix() < state.Until.Unix()
}