import Index from './Index.svelte';
import Projector from './Projector.svelte';

const routes = [
    {
        name: "/",
        component: Index,
    },
    {
        name: "/projector",
        component: Projector
    }
]

export { routes }